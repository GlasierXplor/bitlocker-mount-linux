INSTALL_PATH="/opt/bitlocker-mount"

if [ "$EUID" -ne 0 ]; then
	echo "Script must be run as root"
	exit 1
fi

if [ -f "/usr/local/bin/bitlocker-mount" ]; then
    rm "/usr/local/bin/bitlocker-mount"
fi

if [ -f "/usr/local/bin/bitlocker-umount" ]; then
    rm "/usr/local/bin/bitlocker-umount"
fi

if [ -f "/usr/local/bin/dislocker" ]; then
    cd "$INSTALL_PATH/dislocker"
    make uninstall
fi

echo "Until I figure out a more reliable way to remove the installation directory,"
echo "remove the directory manually by issuing the command below:-"
echo "rm -rf '$INSTALL_PATH'"
