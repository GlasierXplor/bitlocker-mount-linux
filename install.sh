#!/bin/bash

#######################################################################
## Update as necessary

DISLOCKER_LINK="https://github.com/Aorimn/dislocker/archive/v0.7.1.zip"
INSTALL_PATH="/opt/bitlocker-mount"
INSTALL_VAR="$INSTALL_PATH/install_vars"

#######################################################################

if [ "$EUID" -ne 0 ]; then
	echo "Script must be run as root"
	exit 1
fi

BASE_OS="$(cat /etc/os-release)"

if ! echo "$BASE_OS" | grep -qi "ubuntu"; then
  echo "Script only supports Ubuntu based distros!"
  exit 1
fi

echo "### Script to install more scripts to mount Bitlocker protected drives ###"
echo ""
echo "Installation started"

echo ""
echo "Installing dependencies"
if [ "$(command -v apt-get)" ]; then
	echo "Using apt-get"
	echo ""
	echo "apt-get update"
	apt-get update || { echo "No Internet connection!" && exit 1; }
	echo ""
	echo "apt-get install -y gcc cmake make libfuse-dev libmbedtls-dev ruby-dev unzip"
	apt-get install -y gcc cmake make libfuse-dev libmbedtls-dev ruby-dev unzip
fi

if [ $? -ne 0 ]; then
	echo ""
	echo "Dependency installation failed. Check package manager logs"
	exit 1
fi

echo ""
echo ""
sleep 1
echo "Checking if ruby/config.h exists..."
  if [ ! -f /usr/include/ruby-2.5.0/ruby/config.h ]; then
      echo "ruby/config.h does not exist!"
  	echo "Symlinking ruby/config.h to avoid #146 in Dislocker"
  	echo "https://github.com/Aorimn/dislocker/pull/146"
	ln -s /usr/include/x86_64-linux-gnu/ruby-2.5.0/ruby/config.h /usr/include/ruby-2.5.0/ruby/config.h
else
	echo "Symlink/file already exists. Skipping"
fi


sleep 1
echo ""
echo "Creating directory structure"
FAILED=0
if [ ! -d "$INSTALL_PATH" ]; then
	mkdir "$INSTALL_PATH" || FAILED=$FAILED+1
fi
if [ ! -d "$INSTALL_PATH/bin" ]; then
	mkdir "$INSTALL_PATH/bin" || FAILED=$FAILED+1
fi

if [ $FAILED -gt 0 ]; then
    echo "Cannot create directory structure! Cleaning up."
    if [ $FAILED -eq 2 ]; then
        rmdir "$INSTALL_PATH/bin"
    fi
    rmdir "$INSTALL_PATH"
	exit 1
fi

echo "Success"

sleep 1
CWD="$(pwd)"
cd "$INSTALL_PATH" || { echo "cannot cd to $INSTALL_PATH" && exit; }
echo ""
echo "Downloading Dislocker"
wget -nv $DISLOCKER_LINK -O dislocker.zip


if [ $? -ne 0 ]; then
	echo "Dislocker download failed"
	exit 1
fi

echo "Success"
sleep 1
echo ""
echo "Unzipping Dislocker"
unzip -q dislocker.zip
mv dislocker-* dislocker

echo ""

if [ $? -ne 0 ]; then
	echo "Unzipping of Dislocker failed"
	exit 1
fi

echo "Success"
sleep 1
echo ""
echo "Installing Dislocker to /usr/local/bin"
cd dislocker
cmake -D WARN_FLAGS:STRING="-Wall -Wextra" .
make 2>/dev/null
make install


if [ $? -ne 0 ]; then
	echo "Building Dislocker failed"
	exit 1
fi


echo "Success"
sleep 1
echo ""
echo "Copying bitlocker-mount scripts into $INSTALL_PATH/bin"
cp "$CWD/bitlocker-mount" "$CWD/bitlocker-umount" "$INSTALL_PATH/bin"

if [ $? -ne 0 ]; then
	echo "File copy operation failed. Try copying manually:-"
	echo "cp '$CWD/bitlocker-mount' '$CWD/bitlocker-umount' '$INSTALL_PATH/bin'"
	exit 1
fi

echo "Success"
sleep 1
echo ""
echo "Setting permissions (SETUID, executable)"
chown root:root "$INSTALL_PATH/bin/bitlocker-mount"
chown root:root "$INSTALL_PATH/bin/bitlocker-umount"
chmod 4755 "$INSTALL_PATH/bin/bitlocker-mount"
chmod 4755 "$INSTALL_PATH/bin/bitlocker-umount"


sleep 1
echo "Symlinking bitlocker-mount scripts to /usr/local/bin"
ln -s "$INSTALL_PATH/bin/bitlocker-mount" "/usr/local/bin"
ln -s "$INSTALL_PATH/bin/bitlocker-umount" "/usr/local/bin"


if [ $? -ne 0 ]; then
	echo "Symlinking failed. Try symlinking manually by issuing command below:-"
	echo "ln -s '$INSTALL_PATH/bin/*' '/usr/local/bin'"
	exit 1
fi

echo "Success"
sleep 1

echo "Installation completed"
echo "Run 'bitlocker-mount' as root to start."

exit 0
