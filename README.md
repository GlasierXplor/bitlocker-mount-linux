# bitlocker-mount

Repository with Bash scripts to facilitate mounting and unmounting of Bitlocked drives in Linux. This project is built on top of [dislocker](https://github.com/Aorimn/dislocker).

## Install/Uninstall/Update
### Installing
Note: As of now only `apt-get` commands are supported (Debian, Ubuntu, etc).

    git clone https://gitlab.com/GlasierXplor/bitlocker-mount-linux
    chmod +x install.sh
    sudo ./install.sh

### Uninstalling
Note: Dependencies will not be uninstalled lest anything on the OS breaks.

    cd /opt/bitlocker-mount
    chmod +x uninstall.sh
    sudo ./uninstall.sh

### Updating
Note: Updating will always overwrite existing files.

    cd /opt/bitlocker-ount
    chmod +x update.sh
    sudo ./update.sh

## Usage example
### Mounting
To mount partition `/dev/sda2` on `/mnt`,

    sudo bitlocker-mount /dev/sda2 /mnt

Enter password when prompted. The partition's contents will then be accessible via file path `/mnt/unlocked`. It is normal that your file manager or `ls` command complains about not being able to access `/mnt/bitlocked`.

Running `sudo bitlocker-mount` without arguments will print the help prompt, which also includes a list of devices `dislocker-find` detects as Bitlocked devices.

A file `/tmp/bitlocker-mount-dev` will be created and holds a list of partitions and their respective mount points. The mountpoints will not have the `bitlocked` and `unlocked` folder listed. Example of file content:-

    /dev/sda2:/mnt

### Unmounting
To unmount a partition `/dev/sda1` accessible at `/mnt/unlocked`,

    sudo bitlocker-umount /mnt
    # OR
    sudo bitlocker-umount /dev/sda1

`bitlocket-umount` will rely on `/tmp/bitlocker-mount-dev` to decide which mountpoint is mapped to the partition to be unmounted. The partition will then be unmounted and the mountpoint will be cleaned up (folder `bitlocked` and `unlocked` will be removed).

Running `sudo bitlocker-umount` without arguments will print the help prompt, which prints out all the list of partitions mounted and their mountpoints.

# The More Advanced Stuff
## What the scripts do
Note that everything in this repository is in Bash (except for the `README.md` and `LICENSE` files :P ). If in doubt you can always `cat` the files.

### install.sh
  *  Install dependencies using the OS's package manager
  *  If the OS is Ubuntu, copy `ruby/config.h` to the appropriate location
  *  Download the latest release of dislocker (v0.7.1 at time of writing)
  *  Note: **Ubuntu's repositories contain a broken version of dislocker even though the version numbers are the same**
  *  Compile dislocker in `/opt/bitlocker-mount/dislocker`
  *  Install dislocker to `/usr/local/bin/dislocker-*` (default installation location)
  *  Symlink bitlocker-mount scripts from `/opt/bitlocker-mount/bin` to `/usr/local/bin`

### uninstall.sh (WIP)
  *  Remove `bitlocker-mount` scripts in `/usr/local/bin`
  *  `make uninstall` on dislocker
  *  Remove `ruby/config.h` if it was previously symlinked
  *  Remove `/opt/bitlocker-mount` unconditionally

### Dislocker
Keep in mind that all dislocker binaries and commands are fully available to you if you need them

## To Do List
* [ ]  Add check if `dislocker` has been previously installed in `install.sh`
* [ ]  Set `bitlocked` mountpoint to user defined; `unlocked` mountpoint to `/media`
* [ ]  Add `uninstall.sh` script to facilitate uninstallation of bitlocker-mount
* [ ]  Add `update.sh` script to facilitate any updates of bitlocker-mount
~~* [ ]  Add `dnf` support to `install.sh`~~
~~* [ ]  Add `yum` support to `install.sh`~~
* [ ]  Include switches in `install.sh`, `uninstall.sh`, and `update.sh` to customise installation
* [ ]  `install.sh` will copy `uninstall.sh`, and `update.sh` to `/opt/bitlocker-mount`
* [ ]  Add logging to `install.sh`
