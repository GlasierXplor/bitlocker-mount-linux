#!/bin/bash

if [ "$EUID" -ne 0 ]; then
  echo "Run script as root!"
  exit 1
fi

INSTALL_PATH="/opt/bitlocker-mount"

if [ "$(du /tmp/bitlocker-mount-dev | awk '{print $1}')" -ne 0 ]; then
  echo "There are devices still mounted. Unmount them before updating!"
  exit 1
fi 

cd "$INSTALL_PATH" || { echo "$INSTALL_PATH does not exist!" && exit 1; }
git clone https://gitlab.com/GlasierXplor/bitlocker-mount-linux

echo "Overwriting existing files with new ones"
cp "$INSTALL_PATH/bitlocker-mount-linux/bitlocker-mount" "$INSTALL_PATH/bin/"
cp "$INSTALL_PATH/bitlocker-mount-linux/bitlocker-umount" "$INSTALL_PATH/bin/"

echo "Setting permissions (SETUID, executable)"
chown root:root "$INSTALL_PATH/bin/bitlocker-mount"
chown root:root "$INSTALL_PATH/bin/bitlocker-umount"
chmod 4755 "$INSTALL_PATH/bin/bitlocker-mount"
chmod 4755 "$INSTALL_PATH/bin/bitlocker-umount"
